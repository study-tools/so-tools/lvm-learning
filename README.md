# lvm-learning

# Todo LVM tem:
- PV = Phisical Volume (HD / Partição)
- VG = Volume Group
  - PE = Phisical Extents
- LV = Logical Volume (Partição formatada final)

# Vantagem:
- Pode começar com um disco.
- Sempre que for necessário, anexo um novo volume e assim por diante.
- Caso seja feito em dois discos deve-se adicionar dois discos e assim sucessivamente.

# Passo 1:
- Criar estrutura dos discos (Partições):
```shell
cfdisk /dev/sdXYZ
```

![Criar Partições](.img/criar-particoes.png)

# Passo 2:
- Instalar o módulo da LVM.

```shell
yum install lvm2
```

# Passo 3:
- Criar os Phisical Volumes = PV. 

```shel
pvdisplay
pvcreate /dev/sdb1 /dev/sdc1
pvs
pvs -v
```

# Passo 4:
- Criar um Volume Group

```shell
vgdisplay
vgcreate vg01 /dev/sdb1 /dev/sdc1 -s 64M
vgs
vgs -v
```

# Passo 5:
- Criar um grupo

```shell
lvdisplay
lvcreate vg01 -l 100 -n data
lvs
lvs -v
```